import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import settings from '../../settings.js';

let currentCheckStatusRequest = 0;
const maxCheckStatusRequest = 10;

export const uploadFile = createAsyncThunk(
  'files/uploadFile',
  async (filedata, thunkAPI) => {
    const formData = new FormData();
    formData.append("filedata", filedata);

    const response = await axios({
      method: "post",
      url: settings.backendURL + 'api/upload',
      data: formData,
      headers: { 'Content-Type': `multipart/form-data` },
    })

    if (response.data?.status === 0 && response.data?.data?.id) {
      thunkAPI.dispatch(setProcFileIdAC(response.data.data.id));

      thunkAPI.dispatch(calcFileById(response.data.data.id));
    }
    // console.log(response.data);
    return response.data.data.id
  }
)

export const multipleUploadAndFetchFiles = createAsyncThunk(
  'files/multipleUploadFiles',
  async (files, thunkAPI) => {
    try {
      Array.from(files).forEach(async (file, index) => {
        // console.log(index, file);

        const formData = new FormData();
        formData.append("filedata", file);
        thunkAPI.dispatch(setUploadStatusAC(`Отправка ${file.name}`));

        await axios({
          method: "post",
          url: settings.backendURL + 'api/upload',
          data: formData,
          headers: { 'Content-Type': `multipart/form-data` },
        })
          .then(async response => {
            // console.log('from upload ', response.data);
            if (response.data?.status === 0 && response.data?.data?.id) {
              await axios({
                method: "post",
                url: settings.backendURL + "api/calc",
                data: { id: response.data?.data?.id }
              })
                .then(response => {
                  // console.log('from  calc', response.data);
                  if (response.data?.status === 0 && response.data?.data) {
                    // console.log('id ', response.data?.data?.id);

                    fetchStatusByLoopAxios(
                      response.data?.data?.id,
                      () => { },
                      data => {
                        // console.log('from getStatus ', data);
                        const toCompareData = thunkAPI.getState().common.toCompareData;
                        // console.log(toCompareData);
                        thunkAPI.dispatch(setToCompareDataAC([...toCompareData, data.data.result]))

                        if (index === files.length - 1) {
                          thunkAPI.dispatch(setReadyToCompareAC(true));
                        }
                      }
                    )
                  }
                })
            }
          })
          .catch(() => {
            thunkAPI.dispatch(setUploadStatusAC(`Ошибка отправки`));
          })

      })
    } catch (error) {
      console.log(error);
    }

    // return response.data.data.id
  }
)

export const fetchStatusById = createAsyncThunk(
  'files/fetchStatusById',
  async (id, thunkAPI) => {
    const response = await axios({
      method: "get",
      url: settings.backendURL + "api/status?id=" + id,
    })
    if (response.data?.status === 0 && response.data?.data) {
      if (response.data.data?.status === "succes") {
        thunkAPI.dispatch(setResultAC(response.data.data.result));
        thunkAPI.dispatch(setUploadStatusAC("Файл успешно обработан"));
      } else if (response.data.data?.status === "inProgress") {
        thunkAPI.dispatch(setUploadStatusAC("Файл обрабатывается"));
      } else if (response.data.data?.status === "error") {
        thunkAPI.dispatch(setUploadStatusAC("Файл обработался с ошибкой"));
      }
    }

    // console.log(response.data);
    return response.data
  }
)

const fetchStatusByLoopAxios = async (id,
  errCb = () => { },
  resCb = (data) => { },
  inProgCb = (data) => { },
  exceededCb = () => { }
) => {
  const response = await axios({
    method: "get",
    url: settings.backendURL + "api/status?id=" + id,
  })
  currentCheckStatusRequest++;
  if (response.data?.status === 0 && response.data?.data) {

    if (response.data.data?.status === "succes") {
      resCb(response.data);

    } else if (response.data.data?.status === "in progress") {
      inProgCb(response.data);

      if (currentCheckStatusRequest <= maxCheckStatusRequest) {
        setTimeout(() => {
          fetchStatusByLoopAxios(id, errCb, resCb, inProgCb, exceededCb);
        }, 500);
      } else {
        exceededCb();
      }

    } else if (response.data.data?.status === "error") {
      errCb();
    }
  }
  // console.log(response.data);
  return response
}

const fetchStatusByIdLoop = createAsyncThunk(
  'files/fetchStatusByLoop',
  async (id, thunkAPI) => {

    return fetchStatusByLoopAxios(
      id,
      () => {
        thunkAPI.dispatch(setUploadStatusAC("Файл обработался с ошибкой"));
      },
      data => {
        thunkAPI.dispatch(setResultAC(data.data.result));
        if (data.data?.extraInfo) {
          thunkAPI.dispatch(setResExtraInfoAC(data.data.extraInfo))
        }
        if (data.data.result?.length === 0) {
          alert('Результат пустой. Попробуйте поменять настройки');
        }
        thunkAPI.dispatch(setUploadStatusAC("Файл успешно обработан"));
      },
      data => {
        thunkAPI.dispatch(setUploadStatusAC("Файл обрабатывается"));
      },
      () => {
        thunkAPI.dispatch(setUploadStatusAC("Превышен лимит запросов в" + maxCheckStatusRequest));
      }
    )
  }
)

export const calcFileById = createAsyncThunk(
  'files/fetchStatusById',
  async (id, thunkAPI) => {
    const response = await axios({
      method: "post",
      url: settings.backendURL + "api/calc",
      data: { id: id }
    })
    if (response.data?.status === 0 && response.data?.data) {
      if (response.data.data?.status === "in progress") {
        thunkAPI.dispatch(setUploadStatusAC("Файл начал обрабатываться"));
      }
      setTimeout(() => {
        thunkAPI.dispatch(fetchStatusByIdLoop(id));
      }, 1000);

    } else if (response.data.data?.status === "error") {
      thunkAPI.dispatch(setUploadStatusAC("Файл обработался с ошибкой"));
    }

    // console.log(response.data);
    return response.data
  }
)

export const postToCompare = createAsyncThunk(
  'files/postToCompare',
  async (lists, thunkAPI) => {
    thunkAPI.dispatch(setUploadStatusAC("Отправка на сравнение"));

    const response = await axios({
      method: "post",
      url: settings.backendURL + "api/compare/we",
      data: { lists }
    })
    if (response.data?.status === 0 && response.data?.data) {
      thunkAPI.dispatch(setResultComparedAC(response.data.data.result));
      thunkAPI.dispatch(setUploadStatusAC("Сравнение прошло успешно"));
    } else if (response.data.data?.status === "error") {
      thunkAPI.dispatch(setUploadStatusAC("Сравнение прошло с ошибкой"));
    }
    return response.data
  }
)

const initialState = {
  default: "def",
  uploadStatus: '',
  result: null,
  procFileId: '',
  resExtraInfo: null,
  toCompareData: [],
  isFilesCompare: false,
  readyToCompare: false,
  resultCompared: null,
}

export const common = createSlice({
  name: 'common',
  initialState,
  reducers: {
    setDefaultAC: (state, action) => {
      state.default = action.payload;
    },
    setUploadStatusAC: (state, action) => {
      state.uploadStatus = action.payload;
    },
    setResultAC: (state, action) => {
      state.result = action.payload
    },
    setProcFileIdAC: (state, action) => {
      state.procFileId = action.payload
    },
    setResExtraInfoAC: (state, action) => {
      state.resExtraInfo = action.payload
    },
    setToCompareDataAC: (state, action) => {
      state.toCompareData = action.payload
    },
    setIsFilesCompareAC: (state, action) => {
      state.isFilesCompare = action.payload
    },
    setReadyToCompareAC: (state, action) => {
      state.readyToCompare = action.payload
    },
    setResultComparedAC: (state, action) => {
      state.resultCompared = action.payload
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(uploadFile.pending, (state, action) => {
        state.uploadStatus = 'Отправка'
      })
      .addCase(uploadFile.fulfilled, (state, action) => {
        state.uploadStatus = 'Файл отправлен'
      })
      .addCase(uploadFile.rejected, (state, action) => {
        state.uploadStatus = 'Файл не отправлен'
      })
      .addCase(fetchStatusById.pending, (state, action) => {
        state.uploadStatus = 'Проверка результата'
      })
      .addCase(fetchStatusById.rejected, (state, action) => {
        state.uploadStatus = 'Ошибка проверки результата'
      })
  },
})

export const {
  setDefaultAC,
  setUploadStatusAC,
  setResultAC,
  setToCompareDataAC,
  setProcFileIdAC,
  setResExtraInfoAC,
  setIsFilesCompareAC,
  setReadyToCompareAC,
  setResultComparedAC,
} = common.actions;

export const selectDefault = (state) => state.common.default;
export const selectUploadStatus = (state) => state.common.uploadStatus;
export const selectResult = (state) => state.common.result;
export const selectProcFileId = (state) => state.common.procFileId;
export const selectResExtraInfo = (state) => state.common.resExtraInfo;
export const selectIsFilesCompare = (state) => state.common.isFilesCompare;
export const selectCompareData = (state) => state.common.toCompareData;
export const selectReadyToCompare = (state) => state.common.readyToCompare;
export const selectResultCompared = (state) => state.common.resultCompared;

export default common.reducer;