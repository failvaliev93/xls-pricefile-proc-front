import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import config from '../../settings.js';


export const fetchCurrentSettings = createAsyncThunk(
  'settings/fetchCurrent',
  async (data, thunkAPI) => {
    const response = await axios({
      method: "get",
      url: config.backendURL + 'api/settings/current',
    });
    if (response.data?.status === 0 && response.data?.data) {
      thunkAPI.dispatch(setCurrentStAC(response.data.data));

      if (!thunkAPI.getState().settings.initialCurrent) {
        thunkAPI.dispatch(setInitialCurrentStAC(response.data.data));
      }
    }
    // console.log(response.data);
    return response.data
  }
)

export const setCurrentSettingsFromDefault = createAsyncThunk(
  'settings/setCurrentSettingsFromDefault',
  async (key, thunkAPI) => {
    const response = await axios({
      method: "post",
      url: config.backendURL + 'api/settings/current/default',
      data: { key }
    });
    if (response.data?.status === 0) {
      thunkAPI.dispatch(fetchCurrentSettings());
      alert('Настройки восстановлены по умолчанию');
    }
    // console.log(response.data);
    return response.data
  }
)

export const setCurrentSettings = createAsyncThunk(
  'settings/setCurrentSettingsFromDefault',
  async (data, thunkAPI) => {
    const response = await axios({
      method: "post",
      url: config.backendURL + 'api/settings/current',
      data: { key: config.secretKey, ...data }
    });
    if (response.data?.status === 0) {
      thunkAPI.dispatch(fetchCurrentSettings());
      alert('Настройки сохранены');
    }
    // console.log(response.data);
    return response.data
  }
)

const initialState = {
  initialCurrent: null,
  current: null,
  // statusReady: false
}

export const settings = createSlice({
  name: 'settings',
  initialState,
  reducers: {
    setCurrentStAC: (state, action) => {
      state.current = action.payload;
    },
    setInitialCurrentStAC: (state, action) => {
      state.initialCurrent = state.current;
    },
  }
})

export const {
  setCurrentStAC,
  setInitialCurrentStAC
} = settings.actions;

export const selectCurrentSt = (state) => state.settings.current;
export const selectInitialCurrentSt = (state) => state.settings.initialCurrent;

export default settings.reducer;