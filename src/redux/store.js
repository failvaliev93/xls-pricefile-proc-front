import { common } from '../features/common/common.js';
import { configureStore } from '@reduxjs/toolkit';
import { settings } from '../features/settings/settingsSlice.js';

export const store = configureStore({
  reducer: {
    common: common.reducer,
    settings: settings.reducer
  }
})