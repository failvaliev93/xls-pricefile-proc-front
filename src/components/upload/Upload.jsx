import { useSelector, useDispatch } from 'react-redux';
import React, { useEffect, useState } from 'react'
import { multipleUploadAndFetchFiles, selectIsFilesCompare, selectUploadStatus, setIsFilesCompareAC, setReadyToCompareAC, setResExtraInfoAC, setResultAC, setToCompareDataAC, setUploadStatusAC, uploadFile } from '../../features/common/common';
import { Button } from 'react-bootstrap';
import './Upload.css';

const Upload = () => {
  const dispatch = useDispatch();
  const [selectedFiles, setSelectedFiles] = useState(null);
  const [activeBtn, setActiveBtn] = useState(false);
  const statusText = useSelector(selectUploadStatus);

  // const [iscompare, setIscompare] = useState(false);
  const iscompare = useSelector(selectIsFilesCompare);

  useEffect(() => {
    dispatch(setUploadStatusAC('Выберите файл'));
  }, [dispatch]);

  const handleOnClickBtn = () => {
    dispatch(setResultAC(null));
    dispatch(setResExtraInfoAC(null));
    dispatch(setToCompareDataAC([]));
    dispatch(setReadyToCompareAC(false));
    if (iscompare) {
      dispatch(multipleUploadAndFetchFiles(selectedFiles));
    } else {
      dispatch(uploadFile(selectedFiles[0]));
    }
  }

  const handleOnFileChange = (e) => {
    if (iscompare && e.target.files.length < 2) {
      dispatch(setUploadStatusAC('Выберите больше одного файла'));
      setActiveBtn(false);
    } else {
      if (e.target.files[0]) {
        setSelectedFiles(e.target.files);
        setActiveBtn(true);
        dispatch(setUploadStatusAC('Нажмите отправить файл'));
      } else {
        setActiveBtn(false);
      }
    }
  }

  const hanleChangeCompare = is => {
    dispatch(setIsFilesCompareAC(is));
    // setIscompare(is);
    setActiveBtn(false);
    if (!is) {
      dispatch(setUploadStatusAC('Выберите файл'));
    } else {
      dispatch(setUploadStatusAC('Выберите несколько файлов'));
    }
  }

  return (
    <div className='upload'>
      <hr />
      <h3>Загрузка счета</h3>
      <input type="checkbox" name="compare" id="compare" value={iscompare}
        onChange={e => hanleChangeCompare(e.target.checked)}
      />
      <label htmlFor="compare">
        Сравнение файлов
      </label>
      <div className="block">
        {iscompare ?
          <input type="file" id="file" name="filedata"
            onChange={e => { handleOnFileChange(e) }}
            multiple
          />
          :
          <input type="file" id="file" name="filedata"
            onChange={e => { handleOnFileChange(e) }}
          />
        }
        <Button onClick={() => handleOnClickBtn()} disabled={!activeBtn}>
          Отправить на обработку
        </Button>
      </div>
      <p>Состояние: {statusText}</p>
    </div>
  )
}

export default Upload