import './Settings.css';
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { fetchCurrentSettings, selectCurrentSt, selectInitialCurrentSt, setCurrentSettings, setCurrentSettingsFromDefault, setCurrentStAC } from '../../features/settings/settingsSlice';
import { Button } from 'react-bootstrap';


const MyInput = ({ name, value, onChange }) => {
  const [val, setVal] = useState(value);
  const handleOnChange = val => {
    setVal(val);
    onChange(val);
  }

  return (
    <div className='inputPair'>
      <label htmlFor="">{name}</label>
      <input type="text" placeholder={name}
        onChange={e => handleOnChange(e.target.value)} value={val}
      />
    </div>
  )
}

const Settings = () => {
  const dispatch = useDispatch();
  const currentSettings = useSelector(selectCurrentSt);
  const initSettings = useSelector(selectInitialCurrentSt);
  // const [initSettings, setInitSettings] = useState(null);
  // const [currentSettings, setCurrentSettings] = useState(null);
  const [key, setKey] = useState('key');

  useEffect(() => {
    // dispatch(fetchCurrentSettings())
    //   .then((a) => {
    //     const data = a.payload?.data;
    //     if (data) {
    //       setInitSettings(data);
    //       setCurrentSettings(data);
    //       // dispatch(setInitialCurrentStAC(data))
    //     }
    //   })
    dispatch(fetchCurrentSettings())
    // dispatch(setInitialCurrentStAC());
    // .then(() => {
    // setTimeout(() => {
    //   if (currentSettings) {
    //     setStartOfTable(currentSettings?.startOfTable)
    //   }
    // }, 2000)
    // })
  }, [dispatch])

  //Восстановить предыдущие настройки
  const handleClickRestoreStFromInit = () => {
    dispatch(setCurrentStAC(initSettings));
    alert('Настройки восстановлены');
    // .then(v => console.log(v));
  }
  //Восстановить настройки по умолчанию
  const handleClickRestoreStFromDefault = () => {
    dispatch(setCurrentSettingsFromDefault(key));
    dispatch(fetchCurrentSettings());
  }
  //Применить настройки
  const handleClickSaveSt = () => {
    const st = {
      key: key,
      ...currentSettings
    }
    dispatch(setCurrentSettings(st))
  }

  // console.log('asd');

  const stInputs =
    currentSettings ? (
      <>
        <MyInput name="Начало таблицы"
          value={currentSettings.startOfTable}
          onChange={(startOfTable) => dispatch(setCurrentStAC({ ...currentSettings, startOfTable }))}
        />
        <MyInput name="Конец таблицы"
          value={currentSettings.endOfTable}
          onChange={(endOfTable) => dispatch(setCurrentStAC({ ...currentSettings, endOfTable }))}
        />
        <MyInput name="Артикул"
          value={currentSettings.artikulHeader}
          onChange={(artikulHeader) => dispatch(setCurrentStAC({ ...currentSettings, artikulHeader }))}
        />
        <MyInput name="Товар"
          value={currentSettings.productHeader}
          onChange={(productHeader) => dispatch(setCurrentStAC({ ...currentSettings, productHeader }))}
        />
        <MyInput name="Кол-во"
          value={currentSettings.countHeader}
          onChange={(countHeader) => dispatch(setCurrentStAC({ ...currentSettings, countHeader }))}
        />
        <MyInput name="Единица"
          value={currentSettings.unitHeader}
          onChange={(unitHeader) => dispatch(setCurrentStAC({ ...currentSettings, unitHeader }))}
        />
        <MyInput name="Цена"
          value={currentSettings.priceHeader}
          onChange={(priceHeader) => dispatch(setCurrentStAC({ ...currentSettings, priceHeader }))}
        />
        <MyInput name="№ строки в таблице"
          value={currentSettings.cellAdrStart}
          onChange={(cellAdrStart) => dispatch(setCurrentStAC({ ...currentSettings, cellAdrStart }))}
        />
      </>)
      :
      '';

  return (
    <div className='settings'>
      <h3>Найстройки</h3>
      <div>
        {stInputs}
      </div>
      <div className='settingsPanel'>
        <Button variant="secondary" onClick={() => handleClickRestoreStFromDefault()}>
          Восстановить настройки по умолчанию
        </Button>

        <Button variant="secondary" onClick={() => handleClickRestoreStFromInit()}>
          Восстановить предыдущие настройки
        </Button>

        <Button variant="success" onClick={() => handleClickSaveSt()}>
          Применить настройки
        </Button>

        <input type="text" placeholder='кодовое слово'
          onChange={e => setKey(e.target.value)}
          value={key}
        />
      </div>
    </div>
  )
}

export default Settings