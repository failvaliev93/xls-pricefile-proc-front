import React from 'react';
import { useSelector } from 'react-redux';
import { selectIsFilesCompare, selectResExtraInfo, selectResult } from '../../features/common/common';
import Compare from './compare/Compare';
import './ResultStatus.css';

const ResultStatus = () => {
  const result = useSelector(selectResult);
  const extraInfo = useSelector(selectResExtraInfo);
  const iscompare = useSelector(selectIsFilesCompare);

  return (
    <div id='result'>
      {iscompare ?
        <Compare />
        :
        (<><h3>Результат обработки</h3>
          <table id="resultTable">
            <thead>
              <tr>
                <th>Артикул</th>
                <th>Наименование</th>
                <th>Ед. измерения</th>
                <th>Количество</th>
                <th>Сумма</th>
                <th>Коментарий</th>
              </tr>
            </thead>
            <tbody>
              {result && result.map((we, i) =>
              (<tr key={i}>
                <td>{we.artikul ? we.artikul : '-'}</td>
                <td>{we.product ? we.product : '-'}</td>
                <td>{we.unit ? we.unit : '-'}</td>
                <td>{we.count ? we.count : '-'}</td>
                <td>{we.price ? we.price : '-'}</td>
                <td>{we.comment ? we.comment : '-'}</td>
              </tr>
              )
              )}
            </tbody>
          </table>
          <div className="extraInfo">
            {extraInfo?.priceWithNDS && `Сумма с НДС: ${extraInfo.priceWithNDS}`}
          </div>
          <div className="jsonBlock">
            {(result) ?
              <pre>
                {JSON.stringify({ result, extraInfo }, null, 2)}
              </pre>
              :
              <p>Блок с JSON-представлением результата</p>
            }
          </div>
        </>)
      }
    </div>
  )
}

export default ResultStatus