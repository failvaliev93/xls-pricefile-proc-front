
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { postToCompare, selectCompareData, selectReadyToCompare, selectResultCompared, setResultComparedAC } from '../../../features/common/common'
import './Compare.css';
import { diffChars } from 'diff';

const TableRow = ({ num, artikul, product, unit, count, price, className }) => {
  return (
    <tr
      className={className ?? ''}
    >
      <td>{num ?? ''}</td>
      <td>{artikul ?? '-'}</td>
      <td>{product ?? '-'}</td>
      <td>{unit ?? '-'}</td>
      <td>{count ?? '-'}</td>
      <td>{price ?? '-'}</td>
    </tr>
  )
}

const CreateFragment = (t1, t2, simple = false) => {
  try {
    if (!t1 && !t2) return ''
    // if (!t1) return t2
    // if (!t2) return t1
    if (!t1) t1 = '';
    if (!t2) t2 = '';
    t1 = t1.toString().trim();
    t2 = t2.toString().trim();

    if (simple) {
      if (t1 === t2) return <>{t1}</>
      else return <span className='red'>{t2}</span>
    } else {
      if (t1 === t2) return <>{t1}</>
      else {
        const diff = diffChars(t1, t2);
        if (!diff.length) return <>{t1}</>
        return <>
          {diff.map((part, i) => {
            if (part && part.value) {
              let className = part.added ? 'green' : part.removed ? 'red' : '';
              return <span key={new Date() + i} className={className}>{part.value ?? ''}</span>
            }
            return <></>
          })}
        </>
      }
    }
  } catch (error) {
    console.log(error);
  }
}

const ComparedResultTable = () => {
  const resultCompared = useSelector(selectResultCompared);

  return (
    <>
      <table id="resultTable">
        <thead>
          <tr>
            <th>№</th>
            <th>Артикул</th>
            <th>Наименование</th>
            <th>Ед. измерения</th>
            <th>Количество</th>
            <th>Сумма</th>
          </tr>
        </thead>
        <tbody>
          {resultCompared && resultCompared.map((we, i) =>
          (
            <>
              <TableRow
                key={i}
                num={i + 1}
                artikul={we.artikul}
                product={we.product}
                unit={we.unit}
                count={we.count}
                price={we.price}
              />
              {we.similar &&
                (
                  we.similar.map((swe, j) =>
                    <TableRow
                      key={new Date() + j}
                      artikul={CreateFragment(swe.artikul, we.artikul)}
                      product={CreateFragment(swe.product, we.product)}
                      unit={CreateFragment(swe.unit, we.unit, true)}
                      count={CreateFragment(swe.count, we.count, true)}
                      price={CreateFragment(swe.price, we.price, true)}
                      className="inner"
                    />
                  ))
              }
            </>
          )
          )}
        </tbody>
      </table>
    </>
  )
}

const Compare = () => {
  const compareData = useSelector(selectCompareData);
  const readyToCompare = useSelector(selectReadyToCompare);
  const dispatch = useDispatch();


  useEffect(() => {
    if (readyToCompare) {
      dispatch(setResultComparedAC([]))
      dispatch(postToCompare(compareData));
    }
  }, [readyToCompare, dispatch, compareData]);


  return (
    <>
      <h3>Результат сравнения</h3>
      {readyToCompare && <ComparedResultTable />}
    </>
  )
}

export default Compare