import './App.css';
import ResultStatus from './components/result/ResultStatus';
import Settings from './components/settings/Settings';
import Upload from './components/upload/Upload';

function App() {
  return (
    <div className="App">
      <Settings />
      <Upload />
      <ResultStatus />
    </div>
  );
}

export default App;
